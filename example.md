This would be in the haproxy config:
```haproxy
acl whitelist src -f /etc/haproxy/whitelist.txt
```

Then you could update the file and then to update haproxy without restarting it using the update-table.sh script:
```sh
./update-table.sh whitelist.txt
```